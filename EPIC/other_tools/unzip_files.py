import os
import sys
from EPIC.utils import io_tools

def unzipchain(I, wdir):
    f = open(os.path.join(wdir, 'PTchain%i.txt' % (I+1)), 'r')
    g = open(os.path.join(wdir, 'backup chains', 'PTchain%i.txt' % (I+1)), 'w')

    nextline = f.readline()
    n_lines = 0
    while nextline:
        line = nextline.strip().split(', ')
        try:
            deg = int(line[-1])
        except ValueError:
            assert line[-1].endswith('.0')
            deg = int(line[-1][:-2])
        for _ in range(deg):
            n_lines += 1
            g.write(', '.join(line[:-1]) + '\n')
        lastline = line
        nextline = f.readline()
    f.close()
    g.close()

    with open(os.path.join(wdir, 'lastlinePTchain%i.txt' % (I+1)), 'w') as f:
        f.write(', '.join(lastline[:-1]) + '\n')

    print('Finished unzipping chain %i, contains %i states.' % ((I+1), n_lines))

def unzip_chains(wdir, nchains):

    for i in range(nchains):
        unzipchain(i, wdir)

if __name__ == '__main__':
    wdir = sys.argv[1]
    nchains = io_tools.readint(os.path.join(wdir, 'nchains.txt'))
    unzip_chains(wdir, nchains)
