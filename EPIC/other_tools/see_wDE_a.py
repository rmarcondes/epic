import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import collections
import itertools
from observables import wDE1, wDE2, wDE3
from EPIC.utils import io_tools

try:
    import cPickle as pickle
except ImportError:
    import pickle

alpha2s = 0.35
UseTex = True
if UseTex:
    plt.rc('font',**{'family':'serif','serif':['cmr10']}) # 'Times'
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}',]
    plt.rc('text', usetex=True)
plt.rcParams['axes.linewidth'] = 0.5
#plt.rcParams['font.serif'] = 'Palatino'
#plt.rcParams['legend.fontsize'] = 'medium'
plt.rcParams['xtick.top'] = True
plt.rcParams['ytick.right'] = True
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
plt.rcParams['xtick.major.width'] = 0.5
plt.rcParams['ytick.major.width'] = 0.5
plt.rcParams['figure.figsize'] = 4.5, 3.5


listwdir = [arg for arg in sys.argv if os.path.isdir(arg)]

fig, ax = plt.subplots()
wfunction = {
    'FVEoS1':   wDE1,
    'Model 1':   wDE1,
    'FVEoS1b':   wDE1,
    'FVEoS2':   wDE2,
    'Model 2':   wDE2,
    'FVEoS3':   wDE3,
    'Model 3':   wDE3
    }
colors = itertools.cycle(['C%i' % I for I in range(8)])

for wdir in listwdir:
    with open(os.path.join(wdir, '..', 'parnames.p'), 'rb') as f:
        parnames = pickle.load(f)
    BF = np.loadtxt(os.path.join(wdir, 'bf.txt'))
    params = collections.OrderedDict(zip(parnames, BF))

    wdircolor = next(colors)
    wdict = {}
    with open(os.path.join(wdir, 'wDE_list.txt'), 'r') as wDEfile:
        wline = wDEfile.readline()
        while wline:
            a_lims = [float(value) for value in wline.strip().split()]
            try:
                wdict[len(a_lims)].append(a_lims)
            except KeyError:
                wdict[len(a_lims)] = [a_lims,]
            wline = wDEfile.readline()
    for wlen in wdict.keys():
        A_wDEy = np.transpose(wdict[wlen])
        A, wDE_y1, wDE_y2 = A_wDEy[0], A_wDEy[wlen-2], A_wDEy[wlen-1]
        ax.fill_between(A, y1=wDE_y1, y2=wDE_y2, facecolor=wdircolor, linewidth=0.0, alpha=alpha2s)

    inifile = os.listdir(os.path.join(wdir, '..'))
    inifile = [df for df in inifile if not '.swp' in df]
    inifile = io_tools.lookargext('.ini', inifile)
    modelname = io_tools.read1linestring(io_tools.readfile(os.path.join(wdir, '..', inifile)), 'NAME')
    #print(wfunction[modelname].__name__)
    A = np.linspace(np.log(A[0]), np.log(A[-1]), 500)
    A = np.exp(A)
    ax.plot(A, wfunction[modelname](A, params, {}), color=wdircolor, lw=1, ls='-',label=modelname)
    ax.legend(loc='lower left', frameon=False, borderaxespad=0.5, handletextpad=0.8)
    #ax.axvline(1, ls='--')
    #ax.axvline(params['at'], ls='--', color='r', lw=0.5)
    #ax.axhline(-1, ls='--')
    #ax.axhline(wDE1(1, params), ls='-', lw=0.5)

ax.set_xlabel(r'$a$')
ax.set_ylabel(r'$w_d(a)$')
ax.set_xscale('log')
ax.set_ylim(-2., 0.)
ax.set_xlim(1e-2, 4)
fig.tight_layout()
fig.savefig(os.path.join(wdir, 'wDE.pdf'))
