import pickle
import sys
import os

wdir = sys.argv[1]
for picklefile in [
    'mq',
    'nuisance',
    'parnames',
    'priors',
    'tex'
    ]:
    new = pickle.load(open(os.path.join(wdir, picklefile + '.p'), 'rb'))
    pickle.dump(new, open(os.path.join(wdir, picklefile + '.p'), 'wb'), 0)


