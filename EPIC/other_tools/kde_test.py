import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as st
import common
import numpy as np
from EPIC.utils import statistics

# Uniformly distributed sample
def Uniform_sample(a=0, b=1, num=10000):
    return a, b, st.distributions.uniform(a, b-a).rvs(num)

# Normal distributed sample
def Normal_sample(mu=0, sigma=1, num=10000):
    return mu, sigma, st.distributions.norm(mu, sigma).rvs(num)

# Bivariate Normal distributed sample
def BVN_sample(Mu=[0, 0], Cov=[[1, 0], [0, 1]], num=10000):
    return np.array(Mu), np.array(Cov), st.multivariate_normal(Mu, Cov).rvs(num)

def checkdist(sample, bw=None, a=None, b=None, filename='kde1d_test.pdf'):
    support, kde = statistics.make_kde(sample, bandwidth=bw, a=a, b=b)
    fig, ax = plt.subplots()
    ax.hist(sample, bins=20, linewidth=0.5, color='C1', normed=True, alpha=0.3)
    ax.plot(support, kde, linewidth=0.5, color='C0')
    fig.tight_layout()
    fig.savefig(filename)
    return sample.std()/sample.mean()

def checkdist2d(sample, xa=None, xb=None, ya=None, yb=None, filename='kde2d_test.pdf'):
    x, y, kde = statistics.make_kde2d(*sample.transpose(), xa=xa, xb=xb, ya=ya, yb=yb)
    hrange = np.array([[x[0], x[-1]], [y[0], y[-1]]])
    count, xedg, yedg = np.histogram2d(*sample.transpose(), bins=40, range=hrange, normed=True)
    xx, yy = np.meshgrid(xedg, yedg)
    fig, ax = plt.subplots()
    ax.pcolormesh(xx, yy, count.transpose())
    ax.contour(count.transpose(), levels=common.get_sigmalevels(count), extent=hrange.flatten(), colors='orange', linewidths=0.5)
    ax.contour(kde.transpose(), levels=common.get_sigmalevels(kde), extent=hrange.flatten(), colors='cyan', linewidths=0.5)
    fig.tight_layout()
    fig.savefig(filename)


if __name__ == '__main__':

    #_, _, sample1d = Normal_sample()
    #checkdist(sample1d)

    _, _, sample2d = BVN_sample(Cov=[[1, 0.4], [0.4, 1]], num=30000)
    xa, xb = (-0.6, 1.8)
    ya, yb = (-1, 1.2)
    sample2d = np.array([s2 for s2 in sample2d if np.logical_and(
        s2[0] > xa,
        s2[0] < xb,
        ) and np.logical_and(
        s2[1] > ya,
        s2[1] < yb)
        ])
    checkdist2d(sample2d, xa=xa, xb=xb, ya=ya, yb=yb)

    #a, b, sample_flat = Uniform_sample(num=5000)
    #for i in np.arange(0.1, 1.0, 0.05): 
    #    _, _, sample_flat2 = Uniform_sample(a=0.0, b=i, num=int(1000*i))
    #    sample_flat = np.concatenate([sample_flat, sample_flat2])
    #checkdist(sample_flat, a=0, b=1, filename='kde_uniform.pdf')
