import os
os.chdir('/mnt/c/Users/rafae/repo/epicas/EPIC')
import EPIC.cosmology.cosmic_objects as cosmo
import pytest

def test_cde_propto_dm():
    ''' Interacting model DE-DM with Q proportional to DM density'''
    model = cosmo.CosmologicalSetup('cde', interaction_setup={
        'species': ['idm', 'ide'],
        'parameter': {'idm': 'xi'},
        'propto_other': {'ide': 'idm'},
        'tex': r'\xi',
        'sign': {'idm': 1, 'ide': -1},
        }, physical=False, derived='ide')
    assert model.species['idm'].interaction_parameter.label == 'xi'
    assert model.species['ide'].interaction_parameter is None
    assert model.species['ide'].EoS.type == 'constant' 
    assert isinstance(model.species['ide'].density_parameter, cosmo.DerivedParameter) 
    assert model.species['ide'].interacts_with == model.species['idm']
    assert set([par.label for par in model.parameters]) == set([
        'Oc0', 'Od0', 'xi', 'wd', 'H0'])

def test_cde_propto_de():
    ''' Interacting model DE-DM with Q proportional to DE density'''
    model = cosmo.CosmologicalSetup('cde', interaction_setup={
        'species': ['idm', 'ide'],
        'parameter': {'ide': 'xi'},
        'propto_other': {'idm': 'ide'},
        'tex': r'\xi',
        'sign': {'idm': 1, 'ide': -1},
        }, physical=False, derived='ide')
    assert model.species['ide'].interaction_parameter.label == 'xi'
    assert model.species['idm'].interaction_parameter is None
    assert model.species['ide'].EoS.type == 'constant' 
    assert isinstance(model.species['ide'].density_parameter, cosmo.DerivedParameter) 
    assert model.species['idm'].interacts_with == model.species['ide']
    assert set([par.label for par in model.parameters]) == set([
        'Oc0', 'Od0', 'xi', 'wd', 'H0'])

def test_cde_lambda_propto_dm():
    ''' Interacting model DE-DM with w_d = -1 and Q proportional to DM
    density'''
    model = cosmo.CosmologicalSetup('cde lambda', interaction_setup={
        'species': ['idm', 'ilambda'],
        'parameter': {'idm': 'xi'},
        'propto_other': {'ilambda': 'idm'},
        'tex': r'\xi',
        'sign': {'idm': 1, 'ilambda': -1},
        }, physical=False, derived='ilambda')
    assert model.species['idm'].interaction_parameter.label == 'xi'
    assert model.species['ilambda'].interaction_parameter is None
    assert model.species['ilambda'].EoS.type == 'cosmological_constant' 
    assert isinstance(model.species['ilambda'].density_parameter, cosmo.DerivedParameter) 
    assert model.species['ilambda'].interacts_with == model.species['idm']
    assert set([par.label for par in model.parameters]) == set([
        'Oc0', 'Od0', 'xi', 'H0'])

def test_cde_lambda_propto_de():
    ''' Interacting model DE-DM with w_d = -1 and Q proportional to DE
    density'''
    model = cosmo.CosmologicalSetup('cde lambda', interaction_setup={
        'species': ['idm', 'ilambda'],
        'parameter': {'ilambda': 'xi'},
        'propto_other': {'idm': 'ilambda'},
        'tex': r'\xi',
        'sign': {'idm': 1, 'ilambda': -1},
        }, physical=False, derived='ilambda')
    assert model.species['ilambda'].interaction_parameter.label == 'xi'
    assert model.species['idm'].interaction_parameter is None
    assert model.species['ilambda'].EoS.type == 'cosmological_constant' 
    assert isinstance(model.species['ilambda'].density_parameter, cosmo.DerivedParameter) 
    assert model.species['idm'].interacts_with == model.species['ilambda']
    assert set([par.label for par in model.parameters]) == set([
        'Oc0', 'Od0', 'xi', 'H0'])

