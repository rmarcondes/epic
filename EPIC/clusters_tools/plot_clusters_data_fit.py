import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import uncertainties
import common
from observables import th_virial_ratio
import pdb
import sys
import os
import collections
import load_data
from EPIC.utils.number import G, mumH
from EPIC.utils.io_tools import readfile

try:
    import cPickle as pickle
except ImportError:
    import pickle

UseTex = False
if UseTex:
    plt.rc('font',**{'family':'serif','serif':['cmr10']}) # 'Times'
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}', r'\usepackage{siunitx}']
    plt.rc('text', usetex=True)
plt.rcParams['axes.linewidth'] = 0.5
#plt.rcParams['font.serif'] = 'Palatino'
#plt.rcParams['legend.fontsize'] = 'medium'
plt.rcParams['xtick.top'] = True
plt.rcParams['ytick.right'] = True
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
plt.rcParams['xtick.major.width'] = 0.5
plt.rcParams['ytick.major.width'] = 0.5

g = 2

class Measurement(object):
    def __init__(self, line):
        data = line.split()
        self.name = data[0]
        self.point = float(data[1])
        self.obs = float(data[2])
        self.sigma = float(data[3])
        self.VR_ufloat = uncertainties.ufloat(self.obs, self.sigma)

wdir = sys.argv[1]
with open(os.path.join(wdir, '..', 'parnames.p'), 'rb') as f:
    parnames = pickle.load(f)
bf = np.loadtxt(os.path.join(wdir, 'bf.txt'))
params_bf = dict(zip(parnames, bf))

# read from parfits
mu, dev = np.loadtxt(os.path.join(wdir, 'par_fits.txt'), unpack=True)
params_CL = collections.OrderedDict()
for par, m, stddev in zip(parnames, mu, dev):
    params_CL[par] = uncertainties.ufloat(m, stddev)

with open(os.path.join(wdir, '..', 'der_parnames.p'), 'rb') as f:
    der_parnames = pickle.load(f)
der_bf = np.loadtxt(os.path.join(wdir, 'der_pars', 'bf.txt'), unpack=True)
der_params_bf = dict(zip(der_parnames, der_bf))

mu, dev = np.loadtxt(os.path.join(wdir, 'der_par_fits.txt'), unpack=True)
der_params_CL = collections.OrderedDict()
for par, m, stddev in zip(der_parnames, mu, dev):
    der_params_CL[par] = uncertainties.ufloat(m, stddev)

_, _, clusters = load_data.data_clusters('clusters_data')
for cl in clusters:
    #cl.Hubble(params_bf)
    cl.Hubble(params_CL)
    cl.ovr = -3/2*cl.r200/G/cl.M200/1e14 * cl.kTX/ mumH * cl.fc
    cl.dfe = - 1/(2+3*params_CL['xi'])**2 * cl.dlnrhoH2({})
    #cl.dfe = - 1/(2+3*params_bf['xi'])**2 * cl.dlnrhoH2({})

datapoints = [Measurement(data) for data in readfile('clusters_virial_ratio.txt')]

redshifts = np.array([cl.z for cl in clusters])
zmin = min(redshifts)
zmax = max(redshifts)
zrange = zmax - zmin
Z = np.linspace(zmin-0.5*zrange, zmax+0.5*zrange, num=400)

cluster_collection = [cl.name for cl in clusters]

fig, ax = plt.subplots()
ax.errorbar(redshifts, [(cl.ovr - cl.dfe).nominal_value for cl in clusters], [(cl.ovr - cl.dfe).std_dev for cl in clusters], marker='o', mfc='black', markeredgecolor='C3', ecolor='C3', linestyle='', elinewidth=1, markersize=4,  capsize=2, label='TVR')
#ax.errorbar(redshifts-0.001, [cl.ovr.nominal_value for cl in clusters], [cl.ovr.std_dev for cl in clusters], markeredgecolor='C1', marker='o', mfc='black', ecolor='C1', linestyle='', elinewidth=1, markersize=4, capsize=2, label='OVR')
#ax.errorbar(redshifts+0.001, [cl.dfe.nominal_value for cl in clusters], [cl.dfe.std_dev for cl in clusters], marker='o', mfc='black', markeredgecolor='C2', ecolor='C2', linestyle='', elinewidth=1, markersize=4, capsize=2, label='DfE')
ax.legend(loc='lower right', frameon=False)
lims = ax.get_xlim()
ax.axhspan(der_params_CL['TVR'].nominal_value - der_params_CL['TVR'].std_dev, der_params_CL['TVR'].nominal_value + der_params_CL['TVR'].std_dev, alpha=0.4, color='C0', linewidth=0)
ax.axhline(der_params_CL['TVR'].nominal_value, ls='-', color='C0', lw=0.5)
ax.axhline(der_params_bf['TVR'], ls='--', color='C0', lw=0.5)
ax.set_xlim(lims)
#ax.set_ylim(-1, 0)
fig.tight_layout()
fig.savefig(os.path.join(wdir, 'clustersTVR.pdf'))

