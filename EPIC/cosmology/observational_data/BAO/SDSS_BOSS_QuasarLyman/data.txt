# effective redshift 2.36
# anisotropic analysis
# Font-Ribera et al., JCAP 05 (2014) 027
# The redshift is read from the first line. Do not touch it!
# First line is DA/rs, second is c/(H rs)
10.8    0.4
9.0     0.3
