
Welcome to the EPIC user's guide!
=================================

Easy Parameter Inference in Cosmology
(EPIC) is my implementation in Python of a MCMC code for
Bayesian inference of parameters of cosmological models and model comparison via
the computation of Bayesian evidences.

Why EPIC?
---------

I started to develop EPIC as a means of learning how parameter inference can be
made with Markov Chain Monte Carlo, rather than trying to decipher other
codes or using them as black boxes.
The program has fulfilled this purpose and went on to incorporate a few
cosmological observables that I have actually employed in some of my
publications. Now I release this code in the hope it can be useful to
students seeking to learn some of the methods used in Observational Cosmology
and even to use it for their own work.
It still lacks some important features. 
A Boltzmann solver is not available.
I might consider integrate it with CLASS [#CLASS]_ in the future to make it more
useful for more advanced research.
Stay tuned for more.

On the other hand, development is active and with recent versions it is now
possible not only to use EPIC's Cosmology Calculator but also run MCMC
simulations from a nice graphical interface.
You can also check out these other features:

* Cross-platform: the code runs on Python 3 in any operating system.
* EPIC features a Cosmology Calculator that also supports a few models other
  than the standard :math:`\Lambda\text{CDM}` model.
  The list of models include interacting dark energy and some dark energy
  equation-of-state parametrizations.
  The code can output some key distance calculations and compare them across
  different models over a range of redshifts, generating extensively
  customizable plots.
* It uses Python's ``multiprocessing`` library for the evolution of chains in
  parallel in MCMC simulations.
  The separate processes can communicate with each other through some
  ``multiprocessing`` utilities, which made possible the implementation of the
  Parallel Tempering algorithm. [#removed]_ This method is capable of detecting
  and accurately sampling posterior distributions that present two or more
  separated peaks.
* Convergence between independent chains is tested with the multivariate
  version of the Gelman and Rubin test, a very robust method.
* Also, the plots are beautiful and can be customized to a great extent
  directly from the command line or from the graphical interface, without
  having to change the code.  
  You can view triangle plots with marginalized distributions of parameters,
  predefined derived parameters, two-dimensional joint-posterior distributions,
  autocorrelation plots, cross-correlation plots, sequence plots, convergence
  diagnosis and more.

Try it now!

.. rubric:: Footnotes

.. [#CLASS] Lesgourgues, J. "The Cosmic Linear Anisotropy Solving System (CLASS) I: Overview". arXiv:1104.2932 [astro-ph.IM]; Blas, D., Lesgourgues, J., Tram, T. "The Cosmic Linear Anisotropy Solving System (CLASS). Part II: Approximation schemes". Journal of Cosmology and Astroparticle Physics 07 (2011) 034.

.. [#removed] Removed in current version. If you need to use Parallel Tempering, please use version 1.0.4 of EPIC.
