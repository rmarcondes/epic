

The MCMC module
===============

This module is the original part of the program, although massively rewritten
from version 1.0.4 to 1.1.
Originally, EPIC could run with standard MCMC sampler or Parallel-Tempering MCMC.
The former has been temporarily removed to give place to a new and cleaner
implementation attempting to solve some bugs.
Use version 1.0.4 in case you need it.
The MCMC sampler comes with an experimental adaptive routine that adjusts the
covariance of the proposal multivariate Gaussian probability density for
optimal efficiency, aiming at an acceptance rate around :math:`0.234`.
In the following sections I briefly introduce the MCMC method and show how to
use this program to perform simulations, illustrating with examples.

Starting with version 1.3, it is also possible to perform simulations from the
EPIC's GUI.
For long-running simulations it will still be interesting to run from the
terminal, probably on a remote machine. 
But the GUI may be very useful in certain situations, when the user is too
afraind of using the command-line interface and then can run the complete
simulation from the graphical interface or at least use it to prepare the
``.ini`` file to run later from the terminal following the instructions giving
by the program.
Running the complete simulation from the GUI may be impractical if the computer
is being accessed remotely (which usually is the case when your local machine's
processor has only a few cores) or if the simulation takes too long (using data
with tipically expensive likelihood calculations).
However, the unexperienced user may find interesting to watch how the chains
evolve until convergence, since when using the GUI intermediate plots are shown
periodically at every convergence check and the acceptance rates are printed
after every loop when the chains are written to the disk.

..
    In the PT-MCMC (hereafter just PT), the adaptation also adjusts the
    temperature levels of the chains and reduces the number of necessary chains
    if possible.  

.. toctree::

   intro
   beforestart
   MCMC
   GUIMCMC


