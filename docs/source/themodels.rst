.. _modelssec:

The cosmological models
-----------------------

A few models are already implemented. I give a brief description below,
with references for works that discuss some of them in detail and works that
analyzed them with this code. 
The models are objects created from the ``cosmic_objects.CosmologicalSetup`` class.
This class has a generic module ``solve_background`` that calls the ``Fluid``'s
module ``rho_over_rho0`` of each fluid to obtain the solution for their energy
densities. 
When a solution cannot be obtained directly (like in some interacting models),
a fourth-order Runge-Kutta integration is done using the function
``generic_runge_kutta`` from ``EPIC.utils``'s ``integrators`` and the fluids'
``drho_da``.
There is an intermediate function ``get_Hubble_Friedmann`` to calculate the
Hubble rate either by just summing the energy densities, when called from the
Runge-Kutta integration, or calculating them with ``rho_over_rho0``.

Some new models can be introduced in the code just by editing the
``model_recipes.ini``, ``available_species.ini`` and (optionally)
``default_parameter_values.ini`` configuration files, without needing to
rebuild and install the EPIC's package.
The format of the configuration ``.ini`` files is pretty straightforward and
the containing information can serve as a guide for what needs to be defined.

The :math:`\Lambda\text{CDM}` model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When baryons and radiation are included, the solution to this cosmology will
require values for the parameters 
:math:`\Omega_{c0}`,
:math:`\Omega_{b0}`,
:math:`T_{\text{CMB}}`,
:math:`H_0`,
or
:math:`h`,
:math:`\Omega_{c0} h^2`,
:math:`\Omega_{b0} h^2`,
:math:`T_{\text{CMB}}`,
and will find :math:`\Omega_{\Lambda} = 1 - \left( \Omega_{c0} + \Omega_{b0} + \Omega_{r0} \right)` or  
:math:`\Omega_{\Lambda} h^2 = h^2 - \left( \Omega_{c0} h^2 + \Omega_{b0} h^2 + \Omega_{r0} h^2 \right)`
if ``physical``. [#fn_derived]_
The radiation density parameter :math:`\Omega_{r0}` is calculated according to
the CMB temperature :math:`T_{\text{CMB}}`, including the contribution of the
neutrinos (and antineutrinos) of the standard model.
Extending this model to allow curvature is not completely supported yet. The
Friedmann equation is

.. math:: \frac{H(z)}{100 \,\, \text{km s$^{-1}$ Mpc$^{-1}$}} = \sqrt{ (\Omega_{b0} h^2 + \Omega_{c0} h^2) (1+z)^3 + \Omega_{r0} h^2 (1+z)^4 + \Omega_d h^2}

or

.. math:: H(z) = H_0 \sqrt{ (\Omega_{b0} + \Omega_{c0})  (1+z)^3 + \Omega_{r0} (1+z)^4 + \Omega_d},

:math:`H_0` is in units of :math:`\text{km s$^{-1}$ Mpc$^{-1}$}`.
This model is identified in the code by the label ``lcdm``.

The :math:`w\text{CDM}` model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Identified by ``wcdm``, this is like the standard model except that the dark
energy equation of state can be any constant :math:`w_d`, thus having the
:math:`\Lambda\text{CDM}` model as a specific case with :math:`w_d = -1`.
The Friedmann equation is like the above but with the dark energy contribution
multiplied by :math:`(1+z)^{3(1+w_d)}`.

The Chevallier-Polarski-Linder parametrization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The CPL parametrization [#CPL]_ of the dark energy equation of state

.. math::

    w(a) = w_0 + w_a \left( 1-a \right)

is also available. In this case, the dark energy contribution in the Friedmann
equation is multiplied by
:math:`\left(1 + z \right)^{3\left(1 + w_0 + w_a\right)} e^{-3 w_a z /\left(1 + z\right)}` 
or :math:`a^{-3\left(1 + w_0 + w_a\right)} e^{-3 w_a \left(1 - a\right)}`,
in terms of the scale factor.

.. or :math:`(a/a_0)^{-3\left(1 + w_0 + w_a\right)} e^{-3 w_a \left(1 - a/a_0\right)}`,

The Barboza-Alcaniz parametrization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Barboza-Alcaniz dark energy equation of state parametrization [#BA]_

.. math::

    w(z) = w_0 + w_1 \frac{z \left(1 + z\right)}{1 + z^2}

is implemented.
This models gives a dark energy contribution in the Friedmann equation that is
multiplied by the term :math:`x^{3(1+w_0)} \left( x^2 - 2 x + 2 \right)^{-3 w_1/2}`, where :math:`x \equiv 1/a`.

.. multiplied by the term :math:`x^{3(1+w_0)} \left( x^2 - 2 x + 2 \right)^{-3 w_1/2}`, where :math:`x \equiv a_0/a`.

The Jassal-Bagla-Padmanabhan parametrization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Starting with version 1.4, the JBP parametrization [#JBP]_ of the equation of state

.. math::

    w(a) = w_0 + w_1 a (1-a)

can also be used.
In this case, :math:`a^{-3\left(1 + w_0\right)} e^{- 3 w_1 \left[a\left(1 - a/2\right) - 1/2\right]}` 
or :math:`(1+z)^{3\left(1+w_0\right)} e^{3 w_1 z^2/2 \left(1+z\right)^2}`
is the
term that goes into the dark energy contribution in the Friedmann equation.

.. _int-models:

Interacting Dark Energy models
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A comprehensive review of models that consider a possible interaction between
dark energy and dark matter is given by Wang *et al.* (2016) [#WangReview2016]_.
In interacting models, the individual conservation equations of the two dark
fluids are violated, although still preserving the total energy conservation:

.. math::
   
   \dot\rho_c + 3 H \rho_c &= Q \\
   \dot\rho_d + 3 H (1 + w_d) \rho_d &= -Q.

The shape of :math:`Q` is what characterizes each model. Common forms are
proportional to :math:`\rho_c`, to :math:`\rho_d` (both supported) or to some
combination of both (not supported in this version).

.. include::
   ide.rst

Fast-varying dark energy equation-of-state models
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Models of dark energy with fast-varying equation-of-state parameter have been
studied in some works [#fvmodels]_. Three such models were implemented as
described in Marcondes and Pan (2017) [#MarcondesPan2017]_. We used this code
in that work.
They have all the density parameters present in the :math:`\Lambda\text{CDM}`
model besides the dark energy parameters that we describe in the following.

The Linder-Huterer parametrization (Model 1)
""""""""""""""""""""""""""""""""""""""""""""
The model ``lh`` has the free parameters
:math:`w_p`,
:math:`w_f`,
:math:`a_t` and
:math:`\tau` characterizing the equation of state [#LinderHuterer2005]_

.. math:: w_d(a) = w_f + \frac{w_p - w_f}{1 + (a/a_t)^{1/\tau}}.

:math:`w_p` and :math:`w_f` are the asymptotic values of :math:`w_d` in the
past (:math:`a \to 0`) and in the future (:math:`a \to \infty`), respectively;
:math:`a_t` is the scale factor at the transition epoch and :math:`\tau` is the
transition width.
The Friedmann equation is

.. math:: \frac{H(a)^2}{H_0^2} = \frac{\Omega_{r0}}{a^4} + \frac{\Omega_{m0}}{a^3} + \frac{\Omega_{d0}}{a^{3(1+w_p)}} f_1(a),

where

.. math:: f_1(a) = \left( \frac{a^{1/\tau} + a_t^{1/\tau}}{1 + a_t^{1/\tau}} \right)^{3\tau(w_p - w_f)}.

The Felice-Nesseris-Tsujikawa parametrization (Model 2)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""
This FNT model ``fv2`` alters the previous model to allow the dark energy
to feature an extremum value of the equation of state: [#fvmodels]_

.. math:: w_d(a) = w_p + (w_0 - w_p) \, a \, \frac{1 - (a/a_t)^{1/\tau}}{1 - (1/a_t)^{1/\tau}},

where :math:`w_0` is the current value of the equation of state and the other
parameters have the interpretation as in the previous model.
The Friedmann equation is

.. math:: \frac{H(a)^2}{H_0^2} = \frac{\Omega_{r0}}{a^4} + \frac{\Omega_{m0}}{a^3} + \frac{\Omega_{d0}}{a^{3(1+w_p)}} e^{f_2(a)},

with

.. math:: f_2(a) = 3 (w_0 - w_p) \frac{1+ (1- a_t^{-1/\tau})\tau + a \bigl[\bigl\lbrace (a/a_t)^{1/\tau} - 1 \bigr\rbrace \tau - 1 \bigr]}{(1+\tau)(1 - a_t^{-1/\tau})}.

The Felice-Nesseris-Tsujikawa parametrization (Model 3)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""
Finally, we have another FNT model ``fv3`` with the same parameters as in Model 2 but with equation of state [#fvmodels]_

.. math:: w_d(a) = w_p + (w_0 - w_p) \, a^{1/\tau} \, \frac{1 - (a/a_t)^{1/\tau}}{1 - (1/a_t)^{1/\tau}}.

It has a Friedmann equation identical to Model 2's except that :math:`f_2(a)` is replaced by

.. math:: f_3(a) = 3(w_0 - w_p) \tau \frac{2 - a_t^{-1/\tau} + a_t^{1/\tau} \bigl[(a/a_t)^{1/\tau} - 2 \bigr]}{2 \bigl(1 - a_t^{-1/\tau}\bigr)}.


.. rubric:: Footnotes

.. [#fn_derived] That is, assuming ``derived=lambda``, but we could also have done, for example, ``physical=False, derived=matter``, specify :math:`\Omega_{\Lambda}` and the code would get :math:`\Omega_{m0} = 1 - \left( \Omega_{\Lambda} + \Omega_{r0} \right)` or, still, without specifying the derived parameter and with ``physical`` true, specify all the fluids'  density parameters and get :math:`h = \sqrt{\Omega_{c0} h^2 + \Omega_{b0} h^2 + \Omega_{r0} h^2 + \Omega_{\Lambda} h^2}`.

.. [#CPL] Chevallier M. & Polarski D., "Accelerating Universes with scaling dark matter". International Journal of Modern Physics D 10 (2001) 213-223; Linder E. V., "Exploring the Expansion History of the Universe". Physical Review Letters 90 (2003) 091301.

.. [#BA] Barboza E. M. & Alcaniz J. S., "A parametric model for dark energy". Physics Letters B 666 (2008) 415-419.

.. [#JBP] Jassal H. K., Bagla J. S., Padmanabhan T., "*WMAP* constraints on low redshift evolution of dark energy". Monthly Notices of the Royal Astronomical Society 356 (2005) L11-L16; Jassal H. K., Bagla J. S., Padmanabhan T., "Observational constraints on low redshift evolution of dark energy: How consistent are different observations?". Physical Review D 72 (2005) 103503. 

.. [#WangReview2016] Wang B., Abdalla E., Atrio-Barandela F., Pavón D., "Dark matter and dark energy interactions: theoretial challenges, cosmological implications and observational signatures". Reports on Progress in Physics 79 (2016) 096901.

.. [#fvmodels] Corasaniti P. S. & Copeland E. J., "Constraining the quintessence equation of state with SnIa data and CMB peaks". Physical Review D 65 (2002) 043004; Basset B. A., Kunz M., Silk J., "A late-time transition in the cosmic dark energy?". Monthly Notices of the Royal Astronomical Society 336 (2002) 1217-1222; De Felice A., Nesseris S., Tsujikawa S., "Observational constraints on dark energy with a fast varying equation of state". Journal of Cosmology and Astroparticle Physics 1205, 029 (2012).

.. [#MarcondesPan2017] Marcondes R. J. F. & Pan S., "Cosmic chronometer constraints on some fast-varying dark energy equations of state". arXiv:1711.06157 [astro-ph.CO].

.. [#LinderHuterer2005] Linder E. V. & Huterer D., "How many dark energy parameters?". Physical Review D 72 (2005) 043509.
