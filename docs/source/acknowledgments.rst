
Acknowledgments
===============

I want to thank Elcio Abdalla, Gabriel Marcondes and Luiz Irber for their help.
This work has made use of the computing facilities of the Laboratory of
Astroinformatics (IAG/USP, NAT/Unicsul), whose purchase was made possible by
the Brazilian agency FAPESP (grant 2009/54006-4) and the INCT-A.
