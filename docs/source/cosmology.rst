.. _cosmology-module:

The Cosmology calculator
========================

Starting with version 1.1, there is now a module available that makes it easy
for the user to perform calculations on the background cosmology given a
specific model.
A few classes of models are predefined. Each of these have also defined what
are their components, but also allowing some variations. 
For example, the :math:`\Lambda\text{CDM}` model requires at least cold dark
matter (``cdm``) and cosmological constant (``lambda``), but one can also
include baryonic fluid (``baryons``), or treat both matter components together
by specifying the composed species ``matter``.
It is also possible to include ``photons`` or ``radiation``.
For models of interaction between dark matter and dark energy (``cde``), the
former is labelled ``idm`` and the latter ``ide`` or ``ilambda`` in the case
that its equation-of-state parameter is still :math:`-1` (in which the model is
then labelled ``cde_lambda``) rather than a free parameter or presents
evolution described by some function.
The models available and their components specification are defined in the file
``EPIC/cosmology/model_recipes.ini``. The fluids contained in this file are in
turn defined in ``EPIC/cosmology/available_species.ini``, where properties like
the type of equation of state, the parameters and other relevant informations
are set.

In version 1.2, a graphical user interface (GUI) for this cosmology calculator
has been added to EPIC.
After describing how all calculations are done in EPIC, we present the GUI in
the last section.

.. toctree::

   cosmology_module.rst
   themodels.rst
   thedata.rst
   calculator_gui.rst


